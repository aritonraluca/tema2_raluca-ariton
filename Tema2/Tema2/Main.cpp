#include "Wallet.h"
#include <thread>
#include <vector>
#include <iostream>

int main()
{
	int noThreads = 100;
	Wallet wallet;
	std::vector<std::thread> threads;

	for (int index = 0; index < noThreads; index++)
	{
		threads.emplace_back(std::ref(wallet), 15);
	}
	auto treadId = 0;
	for (std::thread& thread : threads)
	{
		std::cout << "join tread: " << treadId << std::endl;
		thread.join();
		treadId++;
	}
	std::cout << wallet.getMoney() << std::endl;
	system("pause");

}