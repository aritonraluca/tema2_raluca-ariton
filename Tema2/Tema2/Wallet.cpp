#include "Wallet.h"
#include <thread>
#include <mutex>


void Wallet::addMoney(int amount)
{
	//static std::mutex mtx;	////////////////
	for (int index = 0; index < amount; index++)
	{
		//std::lock_guard<std::mutex> lockGuard(mtx);	/////////////////////////
		m_money++;
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}

void Wallet::operator()(int amount)
{
	addMoney(amount);
}

int Wallet::getMoney()
{
	return m_money;
}

Wallet::Wallet(int money) :m_money(money)
{
	//Empty
}


Wallet::~Wallet()
{
}
